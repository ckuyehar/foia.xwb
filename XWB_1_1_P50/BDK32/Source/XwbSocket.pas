unit XwbSocket;

interface

uses Trpcb;

type
  IXwbSocket = interface(IInterface)
    function NetCall(hSocket: Integer; imsg: String): PChar;
    function tCall(hSocket: Integer; api, apVer: String; Parameters: TParams;
      var Sec, App: PChar; TimeOut: Integer): PChar;
    function cRight(z: PChar; n: longint): PChar;
    function cLeft(z: PChar; n: longint): PChar;
    function BuildApi(n, p: string; f: longint): string;
    function BuildHdr(wkid: string; winh: string; prch: string;
      wish: string): string;
    function BuildPar(hSocket: Integer; api, RPCVer: string;
      const Parameters: TParams): string;
    function StrPack(n: string; p: Integer): string;
    function VarPack(n: string): string;
    function NetStart(ForegroundM: Boolean; Server: string;
      ListenerPort: Integer; var hSocket: Integer): Integer;
    function NetworkConnect(ForegroundM: Boolean; Server: string;
      ListenerPort, TimeOut: Integer): Integer;
    function libSynGetHostIP(s: string): string;
    function libNetCreate: Integer;
    function libNetDestroy: Integer;
    function GetServerPacket(hSocket: Integer): string;

    procedure NetworkDisconnect(hSocket: Integer);
    procedure NetStop(hSocket: Integer);
    procedure CloseSockSystem(hSocket: Integer; s: string);

    procedure NetError(Action: string; ErrType: Integer);
    function NetStart1(ForegroundM: Boolean; Server: string;
      ListenerPort: Integer; var hSocket: Integer): Integer;
    function BuildPar1(hSocket: Integer; api, RPCVer: string;
      const Parameters: TParams): String;

//    property CountWidth: Integer read FCountWidth write FCountWidth;
//    property IsBackwardsCompatible: Boolean read FIsBackwardsCompatible
//      write FIsBackwardsCompatible;
//    property OldConnectionOnly: Boolean read FOldConnectionOnly
//      write FOldConnectionOnly;
  end;

implementation

end.
